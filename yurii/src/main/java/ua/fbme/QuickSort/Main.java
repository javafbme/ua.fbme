package Yurii.QuickSort;

import java.util.Random;
import java.lang.Math;

/**
 * Created by ��� on 24.02.2016.
 */
public class Main {
    public static void main(String[] args) {
        Random rand = new Random();
        int size = 20;
        int[] arr = new int[size];
        for (int i = 0; i < size; i++)
            arr[i] = rand.nextInt(100);
        print(arr);
        quickSort(arr, size);
        print(arr);
    }

    private static void print(int[] arr) {
        for (int i = 0; i < arr.length; i++)
            System.out.print(arr[i] + " ");
        System.out.println();
    }

    private static void quickSort(int[] arr, int size) {
        if (size > 1) {
            int i_half = 0;
            for (int i = 0; i < size; i++)
                if (Math.abs(0 - arr[i]) < Math.abs(0 - arr[i_half]))
                    i_half = i;
            int N_left = 0;
            for (int i = 0; i < size; i++)
                if (arr[i] < arr[i_half])
                    N_left++;
            int buf = 0;
            buf = arr[i_half];
            arr[i_half] = arr[N_left];
            arr[N_left] = buf;
            for (int i = 0; i < N_left; i++) {
                if (arr[i] >= arr[N_left])
                    continue;
                for (int j = N_left + 1; j < size; j++) {
                    if (arr[j] < arr[N_left])
                        continue;
                    buf = arr[i];
                    arr[i] = arr[j];
                    arr[j] = buf;
                }
            }
            int[] B = new int[N_left];
            for (int i = 0; i < N_left; i++)
                B[i] = arr[i];
            quickSort(B, N_left);
            for (int i = 0; i < N_left; i++)
                arr[i] = B[i];
            B = null;
            B = new int[size - N_left - 1];
            for (int i = N_left + 1, j = 0; i < size; i++, j++)
                B[j] = arr[i];
            quickSort(B, size - N_left - 1);
            for (int i = N_left + 1, j = 0; i < size; i++, j++)
                arr[i] = B[j];
            B = null;
        }
    }
}
