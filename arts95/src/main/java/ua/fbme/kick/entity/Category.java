package ua.fbme.kick.entity;

/**
 * Created by Arsenii on 28.02.2016.
 */
public class Category {
    private String title;
    private String description;


    Category (String n){
        title = n;
    }
    public void setName(String name) {
        this.title = name;
    }
    public String getName() {
        return title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
