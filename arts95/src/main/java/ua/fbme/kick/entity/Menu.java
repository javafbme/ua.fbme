package ua.fbme.kick.entity;

import java.util.Scanner;

/**
 * Created by Arsenii on 28.02.2016.
 */
public class Menu {
    User newUser;
    Project newProject;
    Category newCat;
    Menu(){

    }

    public void setUser() {
        System.out.println("User registration form.");
        Scanner var = new Scanner(System.in);
        System.out.print("Enter password: ");
        String password = var.nextLine();
        System.out.print("Enter email: ");
        String email = var.nextLine();
        newUser = new User(password, email);
        System.out.printf("User created. Email: %s", email);
        System.out.println();
    }
    public void setProject() {
        System.out.println("Project registration form.");
        Scanner var = new Scanner(System.in);
        System.out.print("Enter title of project: ");
        String name = var.nextLine();
        Category cat = createCategory();
        newProject = new Project(name, cat);
        System.out.printf("Project created. Name: %s, Category: %s", name, cat.getName());
        System.out.println();
    }
    public Category createCategory() {
        System.out.println("Category create form.");
        Scanner var = new Scanner(System.in);
        System.out.print("Enter title category: ");
        String name = var.nextLine();
        newCat = new Category(name);
        return newCat;
    }
}
