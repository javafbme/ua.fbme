package ua.fbme.kick.entity;

import java.util.Calendar;

/**
 * Created by Arsenii on 28.02.2016.
 */
public class Project {
    private String title;
    private Category category;
    private Calendar createDate;
    private Calendar editDate;
    Project(String n, Category c){
        title = n;
        category = c;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public Category getCategory() {
        return category;
    }
    public String getTitle() {
        return title;
    }

    public Calendar getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Calendar createDate) {
        this.createDate = createDate;
    }

    public Calendar getEditDate() {
        return editDate;
    }

    public void setEditDate(Calendar editDate) {
        this.editDate = editDate;
    }
}
