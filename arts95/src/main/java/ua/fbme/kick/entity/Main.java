package ua.fbme.kick.entity;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Menu main = new Menu();
        int task = 100;
        while (task != 0) {
            System.out.println("1. New user");
            System.out.println("2. New project");
            System.out.println("0. Exit");
            System.out.print("Enter number: ");
            task = in.nextInt();
            switch (task) {
                case 0: break;
                case 1:
                    main.setUser();
                    break;
                case 2:
                    main.setProject();
                    break;
                default:
                    System.out.println("Error");
                    break;
            }
        }
    }

}
