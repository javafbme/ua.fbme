package ua.fbme.kick.entity;

import javax.persistence.*;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Arsenii on 28.02.2016.
 */
@Entity
@Table(name="USERS")
public class User {
    @Id
    @Column(name="USER_ID")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    private String login;
    private String password;
    private String email;
    //private Role role;
    //private Calendar registrationDate;
    //private Calendar lastVisitDate;
    //private List<Project> projects;

    public User() {}

    public User(String password, String email){
        this.password = password;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setLogin(String login) {
        this.login = login;
    }

/*    public void setLastVisitDate(Calendar lastVisitDate) {
        this.lastVisitDate = lastVisitDate;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public void setRegistrationDate(Calendar registrationDate) {
        this.registrationDate = registrationDate;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Calendar getLastVisitDate() {
        return lastVisitDate;
    }

    public Calendar getRegistrationDate() {
        return registrationDate;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public Role getRole() {
        return role;
    }*/

    public String getEmail() {
        return email;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
