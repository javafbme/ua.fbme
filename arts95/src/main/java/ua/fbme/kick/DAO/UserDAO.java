package ua.fbme.kick.DAO;

import ua.fbme.kick.entity.User;

import java.util.List;

/**
 * Created by Arsenii on 16.03.2016.
 */
public interface UserDAO {
    List<User> list();
    User get(int id);
    void saveOrUpdate(User user);
    void delete(int id);
}