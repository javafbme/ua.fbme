package ua.fbme.kick.dto;

import java.io.Serializable;
/**
 * Created by Arsenii on 11.03.2016.
 */
public class UserDTO {
    private String login;
    private String email;
    private String password;

    public String getPassword() {
        return password;
    }

    public String getLogin() {
        return login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
