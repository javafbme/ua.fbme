package ua.fbme.kick.service;

/**
 * Created by Arsenii on 11.03.2016.
 */
public interface ObjectBuilder<T, U> {
    U build(T object);
}
