package ua.fbme.kick.service;


import ua.fbme.kick.dto.UserDTO;
import ua.fbme.kick.entity.Role;
import ua.fbme.kick.entity.User;

import java.util.Calendar;

/**
 * Created by Arsenii on 11.03.2016.
 */
public class UserBuilder implements ObjectBuilder<UserDTO, User>{
    @Override
    public User build(UserDTO dto) {
        // comment from Kolyan
        // todo add password encryption
        User object = new User(dto.getPassword(), dto.getEmail());

        object.setLogin(dto.getLogin());
/*        object.setRegistrationDate(Calendar.getInstance());
        object.setRole(Role.UNACTIVED);*/
        return object;
    }
}
