public class QuickSort {

    public static void print(int []arr){
        for (int i = 0; i < arr.length; i++){
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }
    public static int []myqsort(int []arr, int left, int right){
        if (left >= right) {
            return arr;
        }
        int i = left,
                j = right,
                m = i - (i - j)/2;
        while (i < j){
            while ((i < m) && (arr[i] <= arr[m])) {
                i++;
            }
            while ((j > m) && (arr[j] >= arr[m])){
                j--;
            }
            if (i < j) {
                int s_p = arr[i];
                arr[i] = arr[j];
                arr[j] = s_p;
                if (i == m) {
                    m = j;
                }
                if (j == m) {
                    m = i;
                }
                myqsort(arr, left, m);
                myqsort(arr, m + 1, right);
            }
        }
        return arr;
    }


    public static void main(String[] args) {
        int[] arr = new int[10];
        for (int i = 0; i < arr.length; i++){
            arr[i] = (int) Math.round(Math.random()*100);
        }
        print(arr);
        myqsort(arr, 0, arr.length-1);
        print(arr);

    }
}
