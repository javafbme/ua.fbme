package ua.fbme;

import java.util.Random;

/**
 * Created by vov4ique on 2/20/2016.
 */
public class quicksort {
    static void printArr(int arr[]) {
        System.out.println();
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }

    static void qs(int arr[], int p, int q) {
        if (p >= q) return;
        int r = arr[p + (q - p) / 2];
        int i = p;
        int j = q;
        while (i <= j) {
            while (arr[i] < r) i++;
            while (arr[j] > r) j--;
            if (i <= j) {
                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
                i++;
                j--;
            }
        }
        if (p < j)
            qs(arr, p, j);
        if (i < q)
            qs(arr, j + 1, q);
    }

    public static void main(String[] args) {
        int n = 9;
        int[] arr = new int[n];
        Random rand = new Random();

        for (int i = 0; i < n; i++) {
            arr[i] = rand.nextInt(15) + 1;
        }

        printArr(arr);
        qs(arr, 0, arr.length - 1);
        printArr(arr);
    }
}
