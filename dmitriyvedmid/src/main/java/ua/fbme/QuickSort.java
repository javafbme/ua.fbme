package ua.fbme;

import java.util.Scanner;

/**
 * Created by Dmitriy on 19-Feb-16.
 */

public class QuickSort {

    public static void main(String[] args) {
        System.out.print("Input array length:");
        Scanner arrsiz = new Scanner(System.in);
        int arrsize = arrsiz.nextInt();
        int[] arr = new int[arrsize];
        System.out.print("Input array elements:");
        for (int i = 0; i < arrsize; i++) {
            Scanner a = new Scanner(System.in);
            arr[i] = a.nextInt();
        }
        int start = 0;
        int end = arr.length - 1;
        sorting(start, end, arr);
        System.out.println("Array:");
        for (int t : arr) {
            System.out.print(t + " ");
        }

    }

    public static void sorting(int start, int end, int[] arr) {
        if (start >= end)
            return;
        int i = start, j = end;
        int cur = i - (i - j) / 2;
        while (i < j) {
            while (i < cur && (arr[i] <= arr[cur])) {
                i++;
            }
            while (j > cur && (arr[cur] <= arr[j])) {
                j--;
            }
            if (i < j) {
                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
                if (i == cur)
                    cur = j;
                else if (j == cur)
                    cur = i;
            }
        }
    }
}