package ua.fbme.blog.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author Nikolay Yashchenko
 */
public class BlogDTO implements Serializable {
    private List<PostDTO> posts;

    public List<PostDTO> getPosts() {
        return posts;
    }

    public void setPosts(List<PostDTO> posts) {
        this.posts = posts;
    }
}
