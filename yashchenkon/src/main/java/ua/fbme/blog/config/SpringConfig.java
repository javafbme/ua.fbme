package ua.fbme.blog.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ua.fbme.blog.SpringApp;

/**
 * @author Nikolay Yashchenko
 */
@Configuration
@ComponentScan(basePackageClasses = SpringApp.class)
public class SpringConfig {

}
