package ua.fbme.blog.web;

import ua.fbme.blog.common.KeyUtil;
import ua.fbme.blog.dao.PostDao;
import ua.fbme.blog.dao.PostDaoImpl;
import ua.fbme.blog.dto.UserDTO;
import ua.fbme.blog.entity.User;
import ua.fbme.blog.service.UserBuilder;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Nikolay Yashchenko
 */
@WebServlet(urlPatterns = "/hello", initParams = @WebInitParam(name = "test", value = "value"))
public class HelloWorldServlet extends HttpServlet {

    private UserBuilder userBuilder = new UserBuilder();
    private PostDao postDao = new PostDaoImpl();

    @Override
    public void init(ServletConfig config) throws ServletException {
        System.out.println(config.getInitParameter("test"));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/views/registration.html").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String fullname = req.getParameter("fullname");
        String email = req.getParameter("email");
        String password = req.getParameter("password");
        UserDTO userDTO = new UserDTO();
        userDTO.setName(fullname);

        //// TODO: 27.03.16
        User build = userBuilder.build(userDTO);
    }
}
