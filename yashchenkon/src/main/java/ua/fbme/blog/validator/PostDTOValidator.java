package ua.fbme.blog.validator;

import ua.fbme.blog.dto.PostDTO;

/**
 * @author Nikolay Yashchenko
 */
public class PostDTOValidator implements Validator<PostDTO> {

    @Override
    public boolean validate(PostDTO object) {
        return object != null &&
                object.getTitle() != null &&
                !object.getTitle().isEmpty() &&
                object.getDescription() != null &&
                !object.getDescription().isEmpty();
    }
}
