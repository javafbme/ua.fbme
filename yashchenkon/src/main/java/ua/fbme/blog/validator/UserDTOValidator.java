package ua.fbme.blog.validator;

import org.springframework.stereotype.Component;
import ua.fbme.blog.dto.UserDTO;

import java.net.InetAddress;

/**
 * @author Nikolay Yashchenko
 */
@Component
public class UserDTOValidator implements Validator<UserDTO> {

    @Override
    public boolean validate(UserDTO object) {
        // todo add better email checking
        return object != null &&
                object.getEmail() != null &&
                !object.getEmail().isEmpty() &&
                object.getPassword() != null &&
                object.getPassword().length() > 5;
    }
}
