package ua.fbme.blog.validator;

/**
 * @author Nikolay Yashchenko
 */
public interface Validator<T> {
    boolean validate(T object);
}
