package ua.fbme.blog.dao;

import ua.fbme.blog.entity.Post;

import java.sql.SQLException;

/**
 * @author Nikolay Yashchenko
 */
public interface PostDao {
    Post create(Post post) throws SQLException;
    Post update(Post post) throws SQLException;
    Post read(Long id) throws SQLException;
    void delete(Post post);
}
