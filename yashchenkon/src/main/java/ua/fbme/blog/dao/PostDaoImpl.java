package ua.fbme.blog.dao;

import org.springframework.stereotype.Repository;
import ua.fbme.blog.entity.Post;

import java.sql.*;

/**
 * @author Nikolay Yashchenko
 */
@Repository
public class PostDaoImpl implements PostDao {

    private Connection connection;
    private PreparedStatement createStatement;
    private PreparedStatement updateStatement;
    private PreparedStatement selectStatement;

    public PostDaoImpl() {
        try {
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "kolyan", "nikolay4444");
            updateStatement = connection.prepareStatement("UPDATE POST SET title=?, description=? WHERE id=?");
            selectStatement = connection.prepareStatement("SELECT * FROM USERS");
            selectStatement.setFetchSize(100);
            CallableStatement callableStatement = connection.prepareCall("{call procedureName(?,?)}");
            callableStatement.setString(1, "Fdsfds");
            callableStatement.setString(2, "fdsfsdf");
            callableStatement.execute();
        } catch (Exception e) {

        } finally {
        }

    }

    @Override
    public Post create(Post post) throws SQLException {
        // dslfnsd
        post.setId(340L);
        createStatement.setLong(1, post.getId());
        createStatement.setString(2, post.getTitle());
        createStatement.setString(3, post.getDescription());
        createStatement.execute();
        return post;
    }

    @Override
    public Post update(Post post) throws SQLException {
        updateStatement.setString(1, post.getTitle());
        updateStatement.setString(2, post.getDescription());
        updateStatement.setLong(3, post.getId());
        updateStatement.execute();
        return post;
    }

    @Override
    public Post read(Long id) throws SQLException {
        return null;
    }

    @Override
    public void delete(Post post) {

    }
}
