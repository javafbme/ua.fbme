package ua.fbme.blog.entity;

import java.util.Calendar;

/**
 * @author Nikolay Yashchenko
 */
public class User {
    private String email;
    private String password;
    private String name;
    private Role role;
    private Calendar registrationDate;
    private Calendar lastVisitDate;
    private String activationKey;
    private Blog blog;

    public User() {}

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Calendar getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Calendar registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Calendar getLastVisitDate() {
        return lastVisitDate;
    }

    public void setLastVisitDate(Calendar lastVisitDate) {
        this.lastVisitDate = lastVisitDate;
    }

    public String getActivationKey() {
        return activationKey;
    }

    public void setActivationKey(String activationKey) {
        this.activationKey = activationKey;
    }

    public Blog getBlog() {
        return blog;
    }

    public void setBlog(Blog blog) {
        this.blog = blog;
    }
}
