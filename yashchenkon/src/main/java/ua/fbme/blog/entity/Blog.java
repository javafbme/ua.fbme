package ua.fbme.blog.entity;

import java.util.Calendar;
import java.util.List;

/**
 * @author Nikolay Yashchenko
 */
public class Blog {
    private Calendar creationDate;
    private List<Post> posts;

    public Calendar getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Calendar creationDate) {
        this.creationDate = creationDate;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }
}
