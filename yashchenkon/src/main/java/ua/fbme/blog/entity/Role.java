package ua.fbme.blog.entity;

/**
 * @author Nikolay Yashchenko
 */
public enum Role {
    UNACTIVED, USER, ADMIN
}
