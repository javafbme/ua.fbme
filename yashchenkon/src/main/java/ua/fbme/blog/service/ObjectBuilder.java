package ua.fbme.blog.service;

/**
 * @author Nikolay Yashchenko
 */
public interface ObjectBuilder<T, U> {
    U build(T object);
}
