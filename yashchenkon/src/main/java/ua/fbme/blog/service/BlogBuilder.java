package ua.fbme.blog.service;

import ua.fbme.blog.dto.BlogDTO;
import ua.fbme.blog.entity.Blog;
import ua.fbme.blog.entity.Post;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Nikolay Yashchenko
 */
public class BlogBuilder implements ObjectBuilder<BlogDTO, Blog> {

    private PostBuilder postBuilder;

    public BlogBuilder(PostBuilder postBuilder) {
        this.postBuilder = postBuilder;
    }

    @Override
    public Blog build(BlogDTO object) {
        Blog blog = new Blog();
        List<Post> posts = object.getPosts().stream()
                .map(postBuilder::build)
                .collect(Collectors.toList());
        blog.setPosts(posts);
        return blog;
    }
}
