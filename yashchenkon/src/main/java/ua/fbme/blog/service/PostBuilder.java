package ua.fbme.blog.service;

import ua.fbme.blog.dto.PostDTO;
import ua.fbme.blog.entity.Post;

/**
 * @author Nikolay Yashchenko
 */
public class PostBuilder implements ObjectBuilder<PostDTO, Post> {

    @Override
    public Post build(PostDTO object) {
        Post post = new Post();
        post.setTitle(object.getTitle());
        post.setDescription(object.getDescription());
        // todo creation date
//        post.setCreationDate();
        return post;
    }
}
