package ua.fbme.blog.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.fbme.blog.common.KeyUtil;
import ua.fbme.blog.dto.UserDTO;
import ua.fbme.blog.entity.Role;
import ua.fbme.blog.entity.User;

import java.util.Calendar;
/**
 * @author Nikolay Yashchenko
 */
@Service
public class UserBuilder implements ObjectBuilder<UserDTO, User> {

    @Autowired
    private KeyUtil keyUtil;

    @Override
    public User build(UserDTO dto) {
        // todo add password encryption
        User object = new User(dto.getEmail(), dto.getPassword());

        object.setName(dto.getName());
        object.setActivationKey(keyUtil.generateKey(dto.getEmail()));
        object.setRegistrationDate(Calendar.getInstance());
        object.setRole(Role.UNACTIVED);
        return object;
    }
}
