package ua.fbme.blog;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ua.fbme.blog.config.SpringConfig;
import ua.fbme.blog.dao.PostDao;
import ua.fbme.blog.dto.UserDTO;
import ua.fbme.blog.service.UserBuilder;

/**
 * @author Nikolay Yashchenko
 */
public class SpringApp {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringConfig.class);
        UserBuilder userBuilder = applicationContext.getBean(UserBuilder.class);
        UserDTO userDTO = new UserDTO();
        userDTO.setEmail("vkont4@gmail.com");
        userBuilder.build(userDTO);
    }
}
