package ua.fbme.blog.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.fbme.blog.dto.UserDTO;
import ua.fbme.blog.entity.User;
import ua.fbme.blog.service.ObjectBuilder;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author Nikolay Yashchenko
 */
@Component
public class KeyUtil {

    @Autowired
    private ObjectBuilder<UserDTO, User> userBuilder;

    public String generateKey(String email) {
        try {
            MessageDigest sha = MessageDigest.getInstance("SHA-256");
            byte[] digest = sha.digest(email.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();

            for (byte aDigest : digest) {
                String hex = Integer.toHexString(0xff & aDigest);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
}
