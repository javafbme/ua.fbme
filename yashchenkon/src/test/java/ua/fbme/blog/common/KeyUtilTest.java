package ua.fbme.blog.common;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Nikolay Yashchenko
 */
public class KeyUtilTest {

    private KeyUtil keyUtil;

    @Before
    public void before() {
        this.keyUtil = new KeyUtil();
    }

    @Test
    public void test() {
        String email = "email@email.com";
        String key = keyUtil.generateKey(email);

        assertNotNull(key);

        String newKey = keyUtil.generateKey(email);
        assertEquals(key, newKey);
    }
}
