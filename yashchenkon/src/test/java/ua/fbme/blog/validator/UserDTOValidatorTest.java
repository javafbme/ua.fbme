package ua.fbme.blog.validator;

import org.junit.Before;
import org.junit.Test;
import ua.fbme.blog.dto.UserDTO;

import static org.junit.Assert.*;

/**
 * @author Nikolay Yashchenko
 */
public class UserDTOValidatorTest {
    private UserDTOValidator userDTOValidator;

    @Before
    public void before() {
        this.userDTOValidator = new UserDTOValidator();
    }

    @Test
    public void testNormalData() {
        UserDTO userDTO = new UserDTO();
        userDTO.setEmail("email@example.com");
        userDTO.setPassword("password");
        assertTrue(userDTOValidator.validate(userDTO));
    }

    @Test
    public void testBadData() {
        UserDTO userDTO = null;
        assertFalse(userDTOValidator.validate(userDTO));

        userDTO = new UserDTO();
        assertFalse(userDTOValidator.validate(userDTO));

        userDTO.setEmail("email@email.com");
        assertFalse(userDTOValidator.validate(userDTO));

        userDTO.setPassword("fdsfs");
        assertFalse(userDTOValidator.validate(userDTO));
    }
}
