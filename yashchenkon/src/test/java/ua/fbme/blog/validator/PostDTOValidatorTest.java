package ua.fbme.blog.validator;

import org.junit.Before;
import org.junit.Test;
import ua.fbme.blog.dto.PostDTO;
import ua.fbme.blog.validator.PostDTOValidator;

import static org.junit.Assert.*;

/**
 * @author Nikolay Yashchenko
 */
public class PostDTOValidatorTest {

    private PostDTOValidator postDTOValidator;

    @Before
    public void before() {
        this.postDTOValidator = new PostDTOValidator();
    }

    @Test
    public void testValidatorNormalData() {
        PostDTO postDTO = new PostDTO();
        postDTO.setTitle("Title");
        postDTO.setDescription("Decs");
        boolean result = postDTOValidator.validate(postDTO);
        assertTrue(result);
    }

    @Test
    public void testBadData() {
        PostDTO postDTO = null;
        assertFalse(postDTOValidator.validate(postDTO));
        postDTO = new PostDTO();
        assertFalse(postDTOValidator.validate(postDTO));
        postDTO.setDescription("Desc");
        assertFalse(postDTOValidator.validate(postDTO));
        postDTO.setTitle("");
        assertFalse(postDTOValidator.validate(postDTO));
        postDTO.setTitle("Title");
        postDTO.setDescription("");
        assertFalse(postDTOValidator.validate(postDTO));
    }
}
