package ua.fbme.blog.service;

import org.junit.Before;
import org.junit.Test;
import ua.fbme.blog.common.KeyUtil;
import ua.fbme.blog.dto.UserDTO;
import ua.fbme.blog.entity.User;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

/**
 * @author Nikolay Yashchenko
 */
public class UserBuilderTest {
    private ObjectBuilder<UserDTO, User> userBuilder;

    @Before
    public void before() {
        this.userBuilder = new UserBuilder();
    }

    @Test
    public void testBuilder() {
        UserDTO userDTO = new UserDTO();
        userDTO.setName("Name");
        userDTO.setPassword("password");
        userDTO.setEmail("email@email.com");

        User user = userBuilder.build(userDTO);
        assertNotNull(user);

        assertThat(user.getName(), equalTo(userDTO.getName()));
        assertThat(user.getPassword(), notNullValue());
        assertThat(user.getEmail(), equalTo(userDTO.getEmail()));
    }
}
