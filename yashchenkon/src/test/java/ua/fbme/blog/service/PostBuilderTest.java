package ua.fbme.blog.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ua.fbme.blog.dto.PostDTO;
import ua.fbme.blog.entity.Post;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;

/**
 * @author Nikolay Yashchenko
 */
public class PostBuilderTest {
    private ObjectBuilder<PostDTO, Post> postBuilder;

    @Before
    public void before() {
        this.postBuilder = new PostBuilder();
    }

    @Test
    public void testBuilder() {
        PostDTO postDTO = new PostDTO();
        postDTO.setTitle("Title");
        postDTO.setDescription("Desc");

        Post post = postBuilder.build(postDTO);

        assertNotNull(post);
        assertThat(post.getTitle(), equalTo(postDTO.getTitle()));
        assertThat(post.getDescription(), equalTo(postDTO.getDescription()));
    }
}
