package ua.fbme;

import java.util.Arrays;

/**
 * Created by Eld on 24.02.2016.
 */

public class QuickSortJ {
    public static void main(String[] args) {
        int[] arr = {10, 3, -8, 1, 13, 5, 7, 9, 0, -3};

        System.out.println("Not sorted Array: " + Arrays.toString(arr));
        QuickSort(arr, 0, 9);
        System.out.println("Quick Sorted Array: " + Arrays.toString(arr));
    }

    public static void QuickSort(int[] arr, int start, int end) {
        int left = start;
        int right = end;

        int point = arr[(left + right) / 2];

        while (left <= right) {

            while (arr[left] < point) {
                left++;
            }

            while (arr[right] > point) {
                right--;
            }

            if (left <= right) {
                swap(arr, left, right);
                left++;
                right--;
            }
        }

        if (start < right) {
            QuickSort(arr, start, right);
        }
        if (left < end) {
            QuickSort(arr, left, end);
        }
    }

    public static void swap(int[] arr, int index1, int index2) {
        int temp = arr[index1];
        arr[index1] = arr[index2];
        arr[index2] = temp;
    }
}