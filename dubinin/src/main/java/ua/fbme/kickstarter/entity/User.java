package ua.fbme.kickstarter.entity;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Daniel on 28.02.2016.
 */
public class User {

    private Long id;
    private String name;
    private String surname;
    private String login;
    private String password;
    private String email;
    private Roles role;
//    private char [] password;
    private ArrayList<Post> post = new ArrayList<Post>();

    public User(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public void setRole(Roles role) {
        this.role = role;
    }

    public ArrayList<Post> getPost() {
        return post;
    }

    public void setPost(ArrayList<Post> post) {
        this.post = post;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Roles getRole() {
        return role;
    }
}
