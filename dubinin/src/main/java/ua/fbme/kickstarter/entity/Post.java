package ua.fbme.kickstarter.entity;

import java.util.Date;
import java.util.Scanner;

/**
 * Created by Daniel on 28.02.2016.
 */
public class Post {

    private Long id;
    private String postname;
    private String category;
    private Date date;
    private String text;

    public Post() {

    }


    public String getPostname() {
        return postname;
    }

    public String getCategory() {
        return category;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setPostname(String postname) {
        this.postname = postname;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
