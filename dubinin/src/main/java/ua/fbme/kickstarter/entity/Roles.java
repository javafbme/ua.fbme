package ua.fbme.kickstarter.entity;

import java.io.Serializable;

/**
 * Created by Daniel on 29.02.2016.
 */
public enum Roles implements Serializable {

    ROLE_USER,
    ROLE_ADMIN,
    ROLE_CLIENT;
}



