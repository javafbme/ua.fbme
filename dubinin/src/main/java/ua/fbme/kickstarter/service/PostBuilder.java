package ua.fbme.kickstarter.service;

import ua.fbme.kickstarter.dto.PostDTO;
import ua.fbme.kickstarter.entity.Post;

/**
 * Created by daniel on 3/28/16.
 */
public class PostBuilder implements ObjectBuilder<PostDTO, Post> {
    @Override
    public Post build(PostDTO object) {
        Post post = new Post();
        post.setPostname(object.getPostname());
        post.setCategory(object.getCategory());
        post.setText(object.getText());
        return post;
    }
}
