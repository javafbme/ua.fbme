package ua.fbme.kickstarter.service;

import ua.fbme.kickstarter.dto.UserDTO;
import ua.fbme.kickstarter.entity.User;

/**
 * Created by daniel on 3/28/16.
 */
public class UserBuilder implements ObjectBuilder<UserDTO, User> {
    @Override
    public User build(UserDTO object) {
        User user = new User();
        user.setName(object.getName());
        user.setSurname(object.getSurname());
        user.setLogin(object.getLogin());
        user.setEmail(object.getEmail());
        user.setPassword(object.getPassword());
        return user;
    }
}
