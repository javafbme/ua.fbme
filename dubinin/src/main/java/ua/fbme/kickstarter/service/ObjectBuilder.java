package ua.fbme.kickstarter.service;

/**
 * Created by daniel on 3/28/16.
 */
public interface ObjectBuilder<T,U> {
    U build(T object);
}
