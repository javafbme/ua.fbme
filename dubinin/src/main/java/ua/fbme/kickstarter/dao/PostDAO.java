package ua.fbme.kickstarter.dao;

import ua.fbme.kickstarter.entity.Post;

import java.sql.SQLException;

/**
 * Created by daniel on 3/28/16.
 */
public interface PostDAO {
    Post create(Post post) throws SQLException;
    Post update(Post post) throws SQLException;
    Post get(Post post) throws SQLException;
    void delete(Long id) throws SQLException;
}
