package ua.fbme.kickstarter.dao;

import ua.fbme.kickstarter.entity.User;

import java.sql.*;

/**
 * Created by Daniel on 27.03.2016.
 */
public class UserDAOImpl implements UserDAO {


    private Connection connection;
    private PreparedStatement createStatement;
    private PreparedStatement updateStatement;
    private PreparedStatement selectStatement;

    public UserDAOImpl(){
        try{
            connection = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/postgres", "postgres", "arvinser");
            updateStatement = connection.prepareStatement("UPDATE USER SET id =?, name=?, surname=?, login=?, email=?, password=? WHERE id=?");
            selectStatement = connection.prepareStatement("SELECT * FROM USERS");
        }catch(Exception e){

        }finally{

        }
    }
    @Override
    public User create(User user) throws SQLException {

        createStatement.setLong(1, user.getId());
        createStatement.setString(2, user.getName());
        createStatement.setString(3, user.getSurname());
        createStatement.setString(4, user.getLogin());
        createStatement.setString(5, user.getEmail());
        createStatement.setString(6, user.getPassword());
        createStatement.execute();
        return user;
    }

    @Override
    public User update(User user) throws SQLException {
        //createStatement.setString(1, user.getId());
        createStatement.setString(2, user.getName());
        createStatement.setString(3, user.getSurname());
        createStatement.setString(4, user.getLogin());
        createStatement.setString(5, user.getEmail());
        createStatement.setString(6, user.getPassword());
        createStatement.execute();
        return user;
    }

    @Override
    public User get(User user) throws SQLException {

        ResultSet rs = selectStatement.executeQuery("SELECT * FROM USERS");
        user.setName(rs.getString(2));
        user.setSurname(rs.getString(3));
        user.setLogin(rs.getString(4));
        user.setEmail(rs.getString(5));
        user.setPassword(rs.getString(6));
        return user;
    }

    @Override
    public void delete(Long id) throws SQLException {
        selectStatement.execute("DELETE USER WHERE id =" + id);
    }
}
