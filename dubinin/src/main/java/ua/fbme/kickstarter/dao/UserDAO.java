package ua.fbme.kickstarter.dao;

import ua.fbme.kickstarter.entity.User;

import java.sql.SQLException;

/**
 * Created by Daniel on 20.03.2016.
 */
public interface UserDAO {

    User create(User user) throws SQLException;
    User update(User user) throws SQLException;
    User get(User user) throws SQLException;
    void delete(Long id) throws SQLException;
}
