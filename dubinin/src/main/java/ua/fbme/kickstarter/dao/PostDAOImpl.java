package ua.fbme.kickstarter.dao;

import ua.fbme.kickstarter.entity.Post;

import java.sql.*;

/**
 * Created by Daniel on 20.03.2016.
 */
public class PostDAOImpl implements PostDAO {

    private Connection connection;
    private PreparedStatement createStatement;
    private PreparedStatement updateStatement;
    private PreparedStatement selectStatement;

    public PostDAOImpl(){
        try{
            connection = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/postgres", "postgres", "arvinser");
            updateStatement = connection.prepareStatement("UPDATE POST SET id=?, postname=?, text=?, text=?, category=? WHERE id=?");
            selectStatement = connection.prepareStatement("SELECT * FROM POST");
        }catch(Exception e){

        }finally{

        }
    }

    @Override
    public Post create(Post post) throws SQLException {

        createStatement.setLong(1, post.getId());
        createStatement.setString(2, post.getPostname());
        createStatement.setString(3, post.getText());
        createStatement.setString(4, post.getCategory());
        createStatement.execute();
        return post;
    }

    @Override
    public Post update(Post post) throws SQLException {
        //createStatement.setLong(1, post.getID());
        updateStatement.setString(2, post.getPostname());
        updateStatement.setString(3, post.getText());
        updateStatement.setString(4, post.getCategory());
        updateStatement.execute();
        return post;
    }

    @Override
    public Post get(Post post) throws SQLException {

        ResultSet rs = selectStatement.executeQuery("SELECT * FROM POST");
        post.setPostname(rs.getString(2));
        post.setText(rs.getString(3));
        post.setCategory(rs.getString(4));
        return post;
    }

    @Override
    public void delete(Long id) throws SQLException {
        selectStatement.execute("DELETE POST WHERE id =" + id);
    }
}
