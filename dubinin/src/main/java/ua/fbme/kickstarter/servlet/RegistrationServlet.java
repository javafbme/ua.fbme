package ua.fbme.kickstarter.servlet;

import ua.fbme.kickstarter.dao.UserDAO;
import ua.fbme.kickstarter.dao.UserDAOImpl;
import ua.fbme.kickstarter.dto.UserDTO;
import ua.fbme.kickstarter.entity.User;
import ua.fbme.kickstarter.service.UserBuilder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by daniel on 3/28/16.
 */
@WebServlet(urlPatterns = "/registration.html", initParams = @WebInitParam(name = "test", value = "value"))
public class RegistrationServlet extends HttpServlet {

    UserBuilder userBuilder = new UserBuilder();
    UserDAO userDAO = new UserDAOImpl();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/views/registration.html").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserDTO userDTO = new UserDTO();
        userDTO.setName(req.getParameter("name"));
        userDTO.setSurname(req.getParameter("surname"));
        userDTO.setEmail(req.getParameter("email"));
        userDTO.setLogin(req.getParameter("login"));
        userDTO.setPassword(req.getParameter("password"));

        User user = userBuilder.build(userDTO);
       try{
           userDAO.create(user);
       }catch(SQLException e){}
    }
}
