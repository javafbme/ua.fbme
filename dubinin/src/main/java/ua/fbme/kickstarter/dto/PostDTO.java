package ua.fbme.kickstarter.dto;

/**
 * Created by daniel on 3/28/16.
 */
public class PostDTO {

    private String postname;
    private String category;
    private String text;

    public String getPostname() {
        return postname;
    }

    public void setPostname(String postname) {
        this.postname = postname;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
