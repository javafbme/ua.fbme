package ua.fbme.kickstarter.entity;

/**
 * Created by Ivan Mikho on 28.03.16.
 */
public class Company {
    private String name;
    private Project[] projects;

    public Company(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
