package ua.fbme.kickstarter.entity;


/**
 * Created by Ivan Mikho on 27.03.16.
 */
class Password {
    private String value;
    Password (String password) {
        value = password;
    }
    public boolean changePassword (String newPassword, String oldPassword) {
        if (oldPassword == value) {
            value = newPassword;
            return true;
        }
        return false;
    }
    public boolean forgotPassword (String email){
        if (true) { //check in DB exist this value
            // post mail with link or new password
            return true;
        }
        return false;
    }
}
public class User {
    private String name;
    private String email;
    private String password;
    private Company company;
    private Long id;

    public User (String name, String email, String password, Company company) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.company = company;
        //UserDao.save(this);

        }

    @Override
    public String toString() {
        return "User #" + id + ":  " + name + ", " + email + ", " + company;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

//    public void setPassword(Password password) {
//        this.password = password;
//    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
