package ua.fbme.kickstarter.entity;

import java.util.Calendar;

/**
 * Created by Ivan Mikho on 27.03.16.
 */
public class Project {
    private String name;
    private String description;
    private Calendar startDate;
    private Calendar finishDate;
    private int moneyGoal;
    private int backersNumber;
    private Company company;

    public Project(String name, String description, Calendar startDate, Calendar finishDate, int moneyGoal, int backersNumber, Company company) {
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.finishDate = finishDate;
        this.moneyGoal = moneyGoal;
        this.backersNumber = backersNumber;
        this.company = company;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Calendar getStartDate() {
        return startDate;
    }

    public void setStartDate(Calendar startDate) {
        this.startDate = startDate;
    }

    public Calendar getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Calendar finishDate) {
        this.finishDate = finishDate;
    }

    public int getMoneyGoal() {
        return moneyGoal;
    }

    public void setMoneyGoal(int moneyGoal) {
        this.moneyGoal = moneyGoal;
    }

    public int getBackersNumber() {
        return backersNumber;
    }

    public void setBackersNumber(int backersNumber) {
        this.backersNumber = backersNumber;
    }
}
