package ua.fbme.kickstarter.dao;

import ua.fbme.kickstarter.entity.Project;

import java.sql.SQLException;

/**
 * Created by Ivan Mikho on 28.03.16.
 */
public interface ProjectDao {
    Project save(Project project) throws SQLException;
    Project get(Project project) throws SQLException;
    Project update(Project project) throws SQLException;
    void delete(Project project) throws SQLException;

}
