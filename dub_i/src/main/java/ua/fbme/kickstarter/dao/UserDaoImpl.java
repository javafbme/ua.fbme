package ua.fbme.kickstarter.dao;

import ua.fbme.kickstarter.entity.Company;
import ua.fbme.kickstarter.entity.User;

import java.sql.*;

/**
 * Created by Ivan Mikho on 28.03.16.
 */
public class UserDaoImpl implements UserDao {
    private Connection connection;
    private PreparedStatement saveStatement;
    private PreparedStatement getStatement;
    private PreparedStatement updateStatement;
    private PreparedStatement deleteStatement;

    public UserDaoImpl() throws SQLException {
        connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "1111");

        saveStatement = connection.prepareStatement("INSERT INTO users VALUES (?,?,?,?) RETURNING id;--",Statement.RETURN_GENERATED_KEYS);
        getStatement = connection.prepareStatement("SELECT * FROM users WHERE id = ?");
        updateStatement = connection.prepareStatement("UPDATE users SET uname = ?, email = ?, password = ?, company = ? WHERE id = ?");
        deleteStatement = connection.prepareStatement("DELETE FROM users WHERE id = ?");
    }

    @Override
    public User save(User user) throws SQLException{
        saveStatement.setString(1,user.getName());
        saveStatement.setString(2,user.getEmail());
        saveStatement.setString(3,user.getPassword());
        saveStatement.setString(4,user.getCompany().toString());
        saveStatement.execute();
        ResultSet rs = saveStatement.getGeneratedKeys();
        if ( rs.next() ) {
            // Retrieve the auto generated key(s).
            long id = rs.getInt(1);
            user.setId(id);
            System.out.println("uhuhu");
        }

        return user;
    }

    @Override
    public User get(long id) throws SQLException {
        getStatement.setLong(1,id);
        ResultSet rs = getStatement.executeQuery();

        if (rs.next()) {
            User user = new User(rs.getString(1), rs.getString(2), rs.getString(3), new Company(rs.getString(4)));
            user.setId((long) rs.getInt(5));
            return user;
        } else {
            return null;
        }

    }

    @Override
    public User update(User user) throws SQLException {
        if (user.getId() != 0) {
            updateStatement.setString(1,user.getName());
            updateStatement.setString(2,user.getEmail());
            updateStatement.setString(3,user.getPassword());
            updateStatement.setString(4,user.getCompany().toString());
            updateStatement.setLong(5,user.getId());
            updateStatement.execute();
        }
        return user;
    }

    @Override
    public void delete(User user) throws SQLException {
        if (user.getId() != 0){
            deleteStatement.setLong(1,user.getId());
        }
    }
}
