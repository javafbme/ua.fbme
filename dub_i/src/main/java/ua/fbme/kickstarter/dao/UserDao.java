package ua.fbme.kickstarter.dao;

import ua.fbme.kickstarter.entity.User;

import java.sql.SQLException;

/**
 * Created by Ivan Mikho on 28.03.16.
 */
public interface UserDao {
    User save (User user) throws SQLException;
    User get (long id) throws SQLException;
    User update (User user) throws SQLException;
    void delete (User user) throws SQLException;
}
