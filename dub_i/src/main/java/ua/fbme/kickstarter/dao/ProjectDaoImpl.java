package ua.fbme.kickstarter.dao;

import ua.fbme.kickstarter.entity.Project;

import java.sql.*;

/**
 * Created by Ivan Mikho on 28.03.16.
 */
public class ProjectDaoImpl implements ProjectDao {
    private Connection connection;
    private PreparedStatement saveStatement;
    private PreparedStatement getStatement;
    private PreparedStatement updateStatement;
    private PreparedStatement deleteStatement;

    public ProjectDaoImpl() throws SQLException {
        connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "1111");

        saveStatement = connection.prepareStatement("INSERT INTO projects VALUES (?,?,?,?) RETURNING id;--", Statement.RETURN_GENERATED_KEYS);
        getStatement = connection.prepareStatement("SELECT * FROM projects WHERE id = ?");
        updateStatement = connection.prepareStatement("UPDATE projects SET uname = ?, email = ?, password = ?, company = ? WHERE id = ?");
        deleteStatement = connection.prepareStatement("DELETE FROM projects WHERE id = ?");
    }

    @Override
    public Project save(Project project) throws SQLException {
        return null;
    }

    @Override
    public Project get(Project project) throws SQLException {
        return null;
    }

    @Override
    public Project update(Project project) throws SQLException {
        return null;
    }

    @Override
    public void delete(Project project) throws SQLException {

    }
}
