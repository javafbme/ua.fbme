package ua.fbme.kickstarter;

import ua.fbme.kickstarter.dao.UserDao;
import ua.fbme.kickstarter.dao.UserDaoImpl;
import ua.fbme.kickstarter.entity.Company;
import ua.fbme.kickstarter.entity.User;

import java.sql.SQLException;

/**
 * Created by Ivan Mikho on 28.03.16.
 */
public class App {
    public static void main(String[] args) throws SQLException {
        Company upmarket = new Company("upmarket");
        User check = new User("Ivan","zvan3256@gmail.com","1111",upmarket);
        UserDao checkDao = new UserDaoImpl();
        checkDao.save(check);
        System.out.println(checkDao.get(3));
        System.out.println(check.getId());
        System.out.println("go lifting!");
    }
}
