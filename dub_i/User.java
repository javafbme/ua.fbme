package ua.fbme.kickstarter;

/**
 * Created by ivan on 26.02.16.
 */
class Password {
    private String value;
    Password (String password) {
        value = password;
    }
    public boolean changePassword (String newPassword, String oldPassword) {
        if (oldPassword == value) {
            value = newPassword;
            return true;
        }
        return false;
    }
    public boolean forgotPassword (String email){
        if (true) { //check in DB exist this value
            // post mail with link or new password
            return true;
        }
        return false;
    }
}
public class User {
    private int id;
    private String firstName;
    private String secondName;
    private String nickName;
    private String email;
    private Password password;
    private String photo;
    User (String firstName, String secondName, String nickName, String email, String passwordVal, String photo) {
        password = new Password (passwordVal);
        this.firstName = firstName;
        this.secondName = secondName;
        this.nickName = nickName;
        this.email = email;
        this.photo = photo;
    }
}
