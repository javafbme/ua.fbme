package ua.fbme;

import java.util.Arrays;
import java.util.Scanner;

import static java.lang.Math.round;

/**
 * Created by Леша on 23.02.2016.
 */
public class QuickSort {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input size array:");
        int size = scanner.nextInt();
        int[] array = new int[size];
        int[] sortArray;

        array = fillArray(array, size);
        System.out.print("Random Array:");
        System.out.print(Arrays.toString(array));

        sortArray = quickSortArray(array, 0, array.length - 1);

        System.out.print("\nSort Array");
        System.out.print(Arrays.toString(sortArray));
    }

    private static int[] quickSortArray(int[] array, int start, int end) {

        if (start >= end) {
            return array;
        }
        int startIndex = start;
        int endIndex = end;
        int referenceIndex = start - (start - end) / 2;

        while (startIndex < endIndex) {
            while ((startIndex < referenceIndex) && (array[startIndex] <= array[referenceIndex])) {
                startIndex++;
            }
            while ((referenceIndex < endIndex) && (array[referenceIndex] <= array[endIndex])) {
                endIndex--;
            }
            if (startIndex < endIndex) {
                int temp = array[startIndex];
                array[startIndex] = array[endIndex];
                array[endIndex] = temp;
                if (startIndex == referenceIndex) {
                    referenceIndex = endIndex;
                } else {
                    if (endIndex == referenceIndex) {
                        referenceIndex = startIndex;
                    }
                }
            }
        }
        quickSortArray(array, start, referenceIndex);
        quickSortArray(array, referenceIndex + 1, end);

        return array;
    }


    private static int[] fillArray(int array[], int size) {
        for (int i = 0; i < size; i++) {
            array[i] = (int) round(Math.random() * 100);
        }
        return array;
    }
}

